$registry = ""
$dockerOrg = ""
$dockerPassword = ""
$dockerUser = ""

function ExecKube($cmd) {
    $exp = 'kubectl ' + $cmd
    Write-Host $exp
    Invoke-Expression $exp
}

$externalDns = & ExecKube -cmd 'get svc ingress-nginx -n ingress-nginx -o=jsonpath="{.status.loadBalancer.ingress[0].ip}"'
Write-Host "Ingress ip detected: $externalDns" -ForegroundColor Yellow 

if (-not [bool]($externalDns -as [ipaddress])) {
    Write-Host "Must install ingress first" -ForegroundColor Red
    Write-Host "Run deploy-ingress.ps1 and  deploy-ingress-azure.ps1" -ForegroundColor Red
    exit
}


# build docker images
$env:DOCKER_REGISTRY_NAME = $registry
docker-compose -p .. -f ../docker-compose.yml build

$services = ("backend-api", "frontend-ui", "auth-api")
if(-not [string]::IsNullOrEmpty($dockerUser))
{
    $registryFDQN = $registry
    docker login -u $dockerUser -p $dockerPassword $registryFDQN

    if (-not $LastExitCode -eq 0) {
        Write-Host "Login failed" -ForegroundColor Red
        exit
    }
    ExecKube -cmd 'config set-context $(kubectl config current-context) --namespace=catsmesh'
    # Try to delete the Docker registry key secret
    ExecKube -cmd 'delete secret docker-registry registry-key'

    # Create the Docker registry key secret
    ExecKube -cmd "create secret docker-registry registry-key --docker-server=$registryFDQN --docker-username=$dockerUser --docker-password=$dockerPassword --docker-email=not@used.com"
} 
foreach($service in $services) {    
    Write-Host "Pushing images to $registry/$service..." -ForegroundColor Yellow
    Write-Host $services
    $imageFqdn = "$registry/$dockerOrg/${service}"
    Write-Host fqdn: $imageFqdn
    docker tag catsmesh/${service}:"latest" ${imageFqdn}:"latest"
    docker push ${imageFqdn}:"latest"
}

# Remove previous service & deployments
ExecKube -cmd 'delete deployments --all  -n catsmesh'
ExecKube -cmd 'delete services --all -n catsmesh'
ExecKube -cmd 'delete configmap internalurls -n catsmesh'
ExecKube -cmd 'delete configmap urls -n catsmesh'
ExecKube -cmd 'delete configmap externalcfg -n catsmesh'


ExecKube -cmd 'create -f catsmesh-namespace.yaml'
ExecKube -cmd 'create -f sql-data.yaml -f keystore-data.yaml --validate=false'
ExecKube -cmd 'create -f internalurls.yaml'
ExecKube -cmd 'create configmap urls --from-literal=frontend_e=http://$($externalDns) --from-literal=backend_e=http://$($externalDns)/backend-api --from-literal=auth_e=http://$($externalDns)/auth-api '

ExecKube -cmd 'label configmap urls app=catsmesh'

ExecKube -cmd 'create -f conf_local.yaml'


# deployments apps
ExecKube -cmd 'create -f backend-api.yaml'
ExecKube -cmd 'create -f frontend.yaml'
ExecKube -cmd 'create -f auth.yaml'