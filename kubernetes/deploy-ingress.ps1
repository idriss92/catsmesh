kubectl apply -f ingresses\frontend-ingress.yaml
# kubectl apply -f ingresses\clientalfredserver-ingress.yaml

# Deploy nginx-ingress core files
kubectl apply -f ingresses\nginx-ingress\namespace.yaml
kubectl apply -f ingresses\nginx-ingress\default-backend.yaml
kubectl apply -f ingresses\nginx-ingress\configmap.yaml 
kubectl apply -f ingresses\nginx-ingress\tcp-services-configmap.yaml
kubectl apply -f ingresses\nginx-ingress\udp-services-configmap.yaml
kubectl apply -f ingresses\nginx-ingress\without-rbac.yaml



