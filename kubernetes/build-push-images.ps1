$registry = ""
$dockerOrg = ""
$dockerPassword = ""
$dockerUser = ""

# build docker images
Write-Host $registry
Write-Host $dockerOrg
Write-Host $dockerPassword
Write-Host $dockerUser

$env:DOCKER_REGISTRY_NAME = $registry
docker-compose -p .. -f ../docker-compose.yml build

$services = ("backend-api", "frontend-ui", "auth-api")
if(-not [string]::IsNullOrEmpty($dockerUser))
{
    $registryFDQN = $registry
    docker login -u $dockerUser -p $dockerPassword $registryFDQN

    if (-not $LastExitCode -eq 0) {
        Write-Host "Login failed" -ForegroundColor Red
        exit
    }
} 
foreach($service in $services) {    
    Write-Host "Pushing images to $registry/$service..." -ForegroundColor Yellow
    Write-Host $services
    $imageFqdn = "$registry/$dockerOrg/${service}"
    Write-Host fqdn: $imageFqdn
    docker tag catsmesh/${service}:"latest" ${imageFqdn}:"latest"
    docker push ${imageFqdn}:"latest"
}
