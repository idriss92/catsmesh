﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;

namespace Auth.Api.Models
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}
