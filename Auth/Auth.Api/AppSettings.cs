﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Api
{
    public class AppSettings
    {
        public string AuthorityUri { get; set; }
        public string FrontEndClient { get; set; }
    }
}
