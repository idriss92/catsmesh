﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Auth.Api.Controllers
{
    public class HomeController : Controller
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IOptionsSnapshot<AppSettings> _settings;
        
        public HomeController(IIdentityServerInteractionService interaction, IOptionsSnapshot<AppSettings> settings)
        {
            _interaction = interaction;
            _settings = settings;
        }

        public IActionResult Index(string returnUrl)
        {
            return View();
        }
    }
}