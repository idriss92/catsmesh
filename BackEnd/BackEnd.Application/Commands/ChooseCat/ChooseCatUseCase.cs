﻿using System;
using System.Threading.Tasks;
using BackEnd.Application.Repositories;

namespace BackEnd.Application.Commands.ChooseCat
{
    public sealed class ChooseCatUseCase: IChooseCatUseCase
    {
        private readonly IVoteWriteOnlyRepository _voteWriteOnlyRepository;

        public ChooseCatUseCase(IVoteWriteOnlyRepository voteWriteOnlyRepository)
        {
            _voteWriteOnlyRepository = voteWriteOnlyRepository;
        }
        public async Task<AddVoteResult> Execute(string catId, Guid userId)
        {
            await _voteWriteOnlyRepository.AddVote(catId, userId);
            return new AddVoteResult();
        }
    }
}
