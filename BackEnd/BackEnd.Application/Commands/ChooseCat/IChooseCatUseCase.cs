﻿using System;
using System.Threading.Tasks;

namespace BackEnd.Application.Commands.ChooseCat
{
    public interface IChooseCatUseCase
    {
        Task<AddVoteResult> Execute(string catId, Guid userId);
    }
}
