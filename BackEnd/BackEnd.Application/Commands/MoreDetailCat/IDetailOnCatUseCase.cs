﻿using System.Threading.Tasks;
using BackEnd.Domain.Objects;

namespace BackEnd.Application.Commands.MoreDetailCat
{
    public interface IDetailOnCatUseCase
    {
        Task<Cat> Execute(string catId);
    }
}
