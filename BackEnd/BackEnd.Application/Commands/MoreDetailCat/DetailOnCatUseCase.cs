﻿using System.Threading.Tasks;
using BackEnd.Application.Repositories;
using BackEnd.Domain.Objects;

namespace BackEnd.Application.Commands.MoreDetailCat
{
    public class DetailOnCatUseCase: IDetailOnCatUseCase
    {
        private readonly IExternalCatReadOnlyRepository _externalCatReadOnlyRepository;

        public DetailOnCatUseCase(IExternalCatReadOnlyRepository externalCatReadOnlyRepository)
        {
            _externalCatReadOnlyRepository = externalCatReadOnlyRepository;
        }

        public async Task<Cat> Execute(string catId)
        {
            return await _externalCatReadOnlyRepository.GetCat(catId);
        }
    }
}
