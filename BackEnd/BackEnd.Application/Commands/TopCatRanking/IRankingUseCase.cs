﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BackEnd.Application.Results;

namespace BackEnd.Application.Commands.TopCatRanking
{
    public interface IRankingUseCase
    {
        Task<IEnumerable<RankingResult>> Execute();
    }
}
