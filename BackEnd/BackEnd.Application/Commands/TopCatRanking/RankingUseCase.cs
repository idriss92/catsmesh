﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Repositories;
using BackEnd.Application.Results;

namespace BackEnd.Application.Commands.TopCatRanking
{
    public sealed class RankingUseCase: IRankingUseCase
    {
        private readonly IVoteReadOnlyRepository _voteReadOnlyRepository;
        private readonly IExternalCatReadOnlyRepository _externalCatReadOnlyRepository;

        public RankingUseCase(IVoteReadOnlyRepository voteReadOnlyRepository, IExternalCatReadOnlyRepository externalCatReadOnlyRepository)
        {
            _voteReadOnlyRepository = voteReadOnlyRepository;
            _externalCatReadOnlyRepository = externalCatReadOnlyRepository;
        }

        public async Task<IEnumerable<RankingResult>> Execute()
        {
            var cats = await _externalCatReadOnlyRepository.GetCats();
            var rankingResults = cats.Select(c => new RankingResult {CatId = c.GeneseId, Url = c.Url, Votes = c.Votes}).ToList();
            var filteredRanking = rankingResults.Where(v => v.Votes > 0).ToList();
            return filteredRanking.OrderBy(v => v.Votes);
        }
    }
}
