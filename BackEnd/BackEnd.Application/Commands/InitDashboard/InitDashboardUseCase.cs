﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Repositories;
using BackEnd.Domain;
using BackEnd.Domain.Objects;
using BackEnd.Domain.ValueObjects;

namespace BackEnd.Application.Commands.InitDashboard
{
    public sealed class InitDashboardUseCase: IInitDashboardUseCase
    {
        private readonly IExternalCatReadOnlyRepository _externalCatReadOnlyRepository;

        public InitDashboardUseCase(IExternalCatReadOnlyRepository externalCatReadOnlyRepository)
        {
            _externalCatReadOnlyRepository = externalCatReadOnlyRepository;
        }

        public async Task<IEnumerable<BiCat>> Execute()
        {
            var coreCenter = new CoreCenter();
            var cats = await _externalCatReadOnlyRepository.GetCats();
            var shuffledCats = coreCenter.ShuffleCats(cats.ToArray());
            return coreCenter.CreateVersus(new Queue<Cat>(shuffledCats));
        }
    }
}
