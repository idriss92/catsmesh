﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BackEnd.Domain.ValueObjects;

namespace BackEnd.Application.Commands.InitDashboard
{
    public interface IInitDashboardUseCase
    {
        Task<IEnumerable<BiCat>> Execute();
    }
}
