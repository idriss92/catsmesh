﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Repositories;
using BackEnd.Application.Results;

namespace BackEnd.Application.Commands.FilteringCatRanking
{
    public class FilteringRankingUseCase: IFilteringRankingUseCase
    {
        private readonly IExternalCatReadOnlyRepository _externalCatReadOnlyRepository;

        public FilteringRankingUseCase(IExternalCatReadOnlyRepository externalCatReadOnlyRepository)
        {
            _externalCatReadOnlyRepository = externalCatReadOnlyRepository;
        }

        public async Task<IEnumerable<RankingResult>> Execute(int numberOfVote)
        {
            var cats = await _externalCatReadOnlyRepository.GetCatsByNumberOfVote(numberOfVote);
            return cats.Select(c => new RankingResult {CatId = c.GeneseId, Url = c.Url, Votes = c.Votes})
                .OrderBy(v => v.Votes);
        }
    
    }
}
