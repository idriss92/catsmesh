﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BackEnd.Application.Results;

namespace BackEnd.Application.Commands.FilteringCatRanking
{
    public interface IFilteringRankingUseCase
    {
        Task<IEnumerable<RankingResult>> Execute(int numberOfVote);
    }
}
