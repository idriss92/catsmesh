﻿using System;
using System.Collections.Generic;
using BackEnd.Domain.ValueObjects;

namespace BackEnd.Application.Results
{
    public class DashboardResult
    {
        public Guid User { get; set; }
        public IEnumerable<BiCat> BiCats { get; set; }

        public DashboardResult(IEnumerable<BiCat> cats, Guid user)
        {
            BiCats = cats;
            User = user;
        }
    }
}
