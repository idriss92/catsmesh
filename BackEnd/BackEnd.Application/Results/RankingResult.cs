﻿namespace BackEnd.Application.Results
{
    public class RankingResult
    {
        public string CatId { get; set; }
        public int Votes { get; set; }
        public string Url { get; set; }
    }
}
