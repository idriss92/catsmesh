﻿using System;
using System.Threading.Tasks;

namespace BackEnd.Application.Repositories
{
    public interface IVoteWriteOnlyRepository
    {
        Task AddVote(string catId, Guid userId);
    }
}
