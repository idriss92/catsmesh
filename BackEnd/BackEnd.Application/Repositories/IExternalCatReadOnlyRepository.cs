﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BackEnd.Domain.Objects;

namespace BackEnd.Application.Repositories
{
    public interface IExternalCatReadOnlyRepository
    {
        Task<IEnumerable<Cat>> GetCats();
        Task<Cat> GetCat(string geneseId);
        Task<IEnumerable<Cat>> GetCatsByNumberOfVote(int numberOfVote);

    }
}
