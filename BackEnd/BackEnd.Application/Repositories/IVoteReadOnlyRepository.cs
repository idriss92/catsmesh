﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BackEnd.Domain.Objects;

namespace BackEnd.Application.Repositories
{
    public interface IVoteReadOnlyRepository
    {
        Task<IEnumerable<Vote>> GetVoteByCatGeneseId(string catId);
        Task<IEnumerable<Vote>> GetVotes();
    }
}
