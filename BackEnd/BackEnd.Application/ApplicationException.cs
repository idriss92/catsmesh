﻿using System;

namespace BackEnd.Application
{
    public class ApplicationException : Exception
    {
        internal ApplicationException(string businessMessage)
            : base(businessMessage)
        {
        }
    }
}
