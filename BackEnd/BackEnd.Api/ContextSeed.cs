﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Infrastructure;
using BackEnd.Infrastructure.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace BackEnd.Api
{
    public class ContextSeed
    {
        public async Task SeedAsync(Context context, IHostingEnvironment env, IOptions<ContextSettings> settings,
            ILogger<ContextSeed> logger)
        {
            var contentRootPath = env.ContentRootPath;

            if (!context.Cats.Any())
            {
                await context.AddRangeAsync(GetCats(contentRootPath, logger));
                await context.SaveChangesAsync();
            }
        }

        IEnumerable<Cat> GetCats(string contentRootPath, ILogger<ContextSeed> seed)
        {
            string catsFile = Path.ChangeExtension(
                typeof(Startup).Assembly.Location,
                "json");
            var content = File.ReadAllText(catsFile);
            var cats = JsonConvert.DeserializeObject<Cat[]>(content);
            return cats.ToList();
        }
    }
}
