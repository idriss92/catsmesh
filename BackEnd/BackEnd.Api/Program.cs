﻿using BackEnd.Infrastructure;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BackEnd.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .MigrateDbContext<Context>((context, provider) =>
                {
                    var env = provider.GetService<IHostingEnvironment>();
                    var settings = provider.GetService<IOptions<ContextSettings>>();
                    var logger = provider.GetService<ILogger<ContextSeed>>();

                    new ContextSeed()
                        .SeedAsync(context, env, settings, logger)
                        .Wait();
                })
                .Run();
        }

        public static IWebHost CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    var builder = new ConfigurationBuilder();
                    builder.AddEnvironmentVariables();
                    config.AddConfiguration(builder.Build());
                })
                .ConfigureLogging((hostingContext, builder) =>
                {
                    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddConsole();
                    builder.AddDebug();
                })
                .Build();
    }
}
