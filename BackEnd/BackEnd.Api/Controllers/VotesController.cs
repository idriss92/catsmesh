﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Commands.ChooseCat;
using BackEnd.Application.Commands.FilteringCatRanking;
using BackEnd.Application.Commands.TopCatRanking;
using BackEnd.Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VotesController : ControllerBase
    {
        private readonly IChooseCatUseCase _chooseService;
        private readonly IRankingUseCase _rankingService;
        private readonly IFilteringRankingUseCase _rankingFilteringService;
        private readonly IAuthService _authService;
        public VotesController(IChooseCatUseCase chooseService, IRankingUseCase rankingService, IFilteringRankingUseCase rankingFilteringService, IAuthService authService)
        {
            _chooseService = chooseService;
            _rankingService = rankingService;
            _rankingFilteringService = rankingFilteringService;
            _authService = authService;
        }


        // POST: api/Votes
        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> Post([FromBody] string choosenCatId, [FromHeader(Name = "x-requestId")]string requestId)
        {
            var userId = _authService.GetUserIdentity();
            var parsedUserId = (Guid.TryParse(userId, out Guid guid) && guid != Guid.Empty) ? guid : Guid.NewGuid();

            var voteResult = await _chooseService.Execute(choosenCatId, parsedUserId);

            if (voteResult == null)
            {
                return new NoContentResult();
            }
            
            return new ObjectResult(voteResult);
        }

        // GET: api/Votes
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var rankings = await _rankingService.Execute();
            if (rankings.ToList().Count == 0)
                return new NoContentResult();
            return new ObjectResult(rankings);
        }


        // GET: api/Votes/5
        [HttpGet("{numberOfVote}", Name = "Get")]
        public async Task<IActionResult> Get(int numberOfVote)
        {
            var rankings = await _rankingFilteringService.Execute(numberOfVote);

            return Ok(rankings);
        }

    }
}
