﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BackEnd.Application.Commands.InitDashboard;
using BackEnd.Application.Commands.MoreDetailCat;
using BackEnd.Domain.Objects;
using BackEnd.Domain.ValueObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CatsController : ControllerBase
    {
        private readonly IInitDashboardUseCase _initDashboardService;
        private readonly IDetailOnCatUseCase _detailOnCatService;

        public CatsController(IInitDashboardUseCase initDashboardService, IDetailOnCatUseCase detailOnCatService)
        {
            _initDashboardService = initDashboardService;
            _detailOnCatService = detailOnCatService;
        }

        // GET: api/Cats
        [HttpGet]
        public async Task<IEnumerable<BiCat>> Get()
        {
            var dashboard = await _initDashboardService.Execute();
            return dashboard;
        }

        // GET: api/Cats/5
        [HttpGet("{catId}", Name = "GetCat")]
        public async Task<Cat> Get(string catId)
        {
            var res = await _detailOnCatService.Execute(catId);
            return res;
        }

    }
}
