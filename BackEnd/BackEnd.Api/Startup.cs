﻿using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using BackEnd.Application.Commands.ChooseCat;
using BackEnd.Application.Commands.FilteringCatRanking;
using BackEnd.Application.Commands.InitDashboard;
using BackEnd.Application.Commands.MoreDetailCat;
using BackEnd.Application.Commands.TopCatRanking;
using BackEnd.Application.Repositories;
using BackEnd.Infrastructure;
using BackEnd.Infrastructure.Filters;
using BackEnd.Infrastructure.Repositories;
using BackEnd.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace BackEnd.Api
{
    public sealed class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => { options.Filters.Add(typeof(ValidateModelStateFilter)); })
                .AddControllersAsServices();
            services.Configure<ContextSettings>(Configuration);
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("AuthUrl");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = identityUrl;
                options.RequireHttpsMetadata = false;
                options.Audience = "backend";
            });


            // services.AddCors(options =>
            // {
            //     options.AddPolicy("CorsPolicy",
            //         builder => builder.AllowAnyOrigin()
            //             .AllowAnyMethod()
            //             .AllowAnyHeader()
            //             .AllowCredentials());
            // });


            services.Configure<ContextSettings>(Configuration);

            ConfigureDependencies(services);

            services.AddSwaggerGen(options =>
                        {
                            options.DescribeAllEnumsAsStrings();

                            options.IncludeXmlComments(
                                Path.ChangeExtension(
                                    typeof(Startup).Assembly.Location,
                                    "xml"));

                            options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                            {
                                Title = Configuration["App:Title"],
                                Version = Configuration["App:Version"],
                                Description = Configuration["App:Description"],
                                TermsOfService = Configuration["App:TermsOfService"]
                            });

                            options.CustomSchemaIds(x => x.FullName);

                            options.OperationFilter<AuthorizeCheckOperationFilter>();
                        });

            //.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            //services.AddAuthorization();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                app.UsePathBase(pathBase);
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //else
            //{
            //    app.UseHsts();
            //}

            //app.UseHttpsRedirection();

            app.UseCors("CorsPolicy");
            app.UseAuthentication();

            app.UseMvc();

            app.UseSwagger(c => { c.RouteTemplate = "api-docs/{documentName}/swagger.json"; })
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/api-docs/v1/swagger.json", "Backend Api");
                    c.RoutePrefix = "api-docs";
                });
        }

        public void ConfigureDependencies(IServiceCollection services)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            // context
            services.AddDbContext<Context>(c =>
            {
                try
                {
                    var settings = services.BuildServiceProvider().GetService<IOptions<ContextSettings>>().Value;
                    c.UseSqlServer(settings.ConnectionString);
                    c.ConfigureWarnings(wb =>
                    {
                        //By default, in this application, we don't want to have client evaluations
                        wb.Log(RelationalEventId.QueryClientEvaluationWarning);
                    });
                }
                catch (System.Exception ex)
                {
                    var message = ex.Message;
                }
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAuthService, AuthService>();

            // repositories
            services.AddTransient<IExternalCatReadOnlyRepository, CatsRepository>();
            services.AddTransient<IVoteWriteOnlyRepository, VotesRepository>();
            services.AddTransient<IVoteReadOnlyRepository, VotesRepository>();

            //Use cases
            services.AddTransient<IInitDashboardUseCase, InitDashboardUseCase>();
            services.AddTransient<IChooseCatUseCase, ChooseCatUseCase>();
            services.AddTransient<IDetailOnCatUseCase, DetailOnCatUseCase>();
            services.AddTransient<IRankingUseCase, RankingUseCase>();
            services.AddTransient<IFilteringRankingUseCase, FilteringRankingUseCase>();
        }

    }
}
