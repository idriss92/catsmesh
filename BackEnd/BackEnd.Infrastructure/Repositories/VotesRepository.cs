﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Repositories;
using BackEnd.Domain.Objects;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Infrastructure.Repositories
{
    public class VotesRepository:IVoteWriteOnlyRepository, IVoteReadOnlyRepository
    {
        private readonly Context _context;

        public VotesRepository(Context context)
        {
            _context = context;
        }
        public async Task AddVote(string geneseId, Guid userId)
        {
            var cat = await _context.Cats.SingleOrDefaultAsync(c => c.GeneseIdentifier.Equals(geneseId));
            if(cat== null)
                return;
            _context.Add(new Entities.Vote(cat, userId));
            await _context.SaveChangesAsync();
        }
        
        public async Task<IEnumerable<Vote>> GetVoteByCatGeneseId(string geneseId)
        {
            var votes = _context.Votes.Include("Cat").Where(x => x.Cat.GeneseIdentifier == geneseId).ToList();

            return votes.Select(v => Vote.Load(v.Id, v.CatId, v.VoteDateUtc));
        }

        public async Task<IEnumerable<Vote>> GetVotes()
        {
            var votes = await _context.Votes.Include("Cat").ToListAsync();

            return votes.Select(v => Vote.Load(v.Id, v.CatId, v.VoteDateUtc));
        }
    }
}
