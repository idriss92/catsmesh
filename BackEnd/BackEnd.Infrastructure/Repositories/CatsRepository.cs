﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Repositories;
using BackEnd.Domain;
using BackEnd.Domain.Objects;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Infrastructure.Repositories
{
    public class CatsRepository: IExternalCatReadOnlyRepository
    {
        private readonly Context _context;

        public CatsRepository(Context context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Cat>> GetCats()
        {
            var cats = await _context.Cats.Include("Votes").ToListAsync();
            if (cats.Count == 0)
            {
                throw new DomainException("No cats to retrieve");
            }

            return cats.Select(c =>
            {
                var votes = new VoteCollection();
                votes.Add(c.Votes);
                return Cat.Load(c.Id, c.GeneseIdentifier, c.Url, votes);
            });
        }


        public async Task<Cat> GetCat(string geneseId)
        {
            var cat = await _context.Cats.Include("Votes").SingleOrDefaultAsync(c => c.GeneseIdentifier.Equals(geneseId));
            if (cat == null)
            {
                throw new DomainException($"Unable to get the cat with identifier equals to {geneseId}");
            }

            return new Cat(cat.Id, cat.GeneseIdentifier, cat.Url);
        }

        public async Task<IEnumerable<Cat>> GetCatsByNumberOfVote(int numberOfVote)
        {
            var cats = await _context.Cats.Include("Votes").Where(c => c.Votes.Count == numberOfVote).ToListAsync();
            if (cats.Count == 0)
            {
                throw new DomainException("No cats to retrieve");
            }

            return cats.Select(c =>
            {
                var votes = new VoteCollection();
                votes.Add(c.Votes);
                return Cat.Load(c.Id, c.GeneseIdentifier, c.Url, votes);
            });
        }
    }
}
