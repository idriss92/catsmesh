﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BackEnd.Infrastructure.Entities
{
    public class Cat
    {
        public Cat()
        {
            Votes = new List<Vote>();
        }
        [Key]
        public Guid Id { get; set; }
        [JsonProperty("id")]
        public string GeneseIdentifier { get; set; }

        public string Url { get; set; }

        public virtual List<Vote> Votes { get; set; }
    }
}
