﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BackEnd.Domain.Objects;

namespace BackEnd.Infrastructure.Entities
{
    public class Vote: IVote
    {
        public Vote()
        {
            
        }

        public Vote(Cat cat, Guid userId)
        {
            VoteDateUtc = DateTime.UtcNow;
            Cat = cat;
            CatId = cat.Id;
            UserId = userId;
        }
        [Key]
        public Guid Id { get; set; }
        
        public DateTime VoteDateUtc { get; set; }
        public Guid UserId { get; set; }

        [ForeignKey("Cat")] public Guid CatId { get; set; }
        public Cat Cat { get; set; }
    }
}
