﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackEnd.Infrastructure.Services
{
    public interface IAuthService
    {
        string GetUserIdentity();

        string GetUserName();
    }
}
