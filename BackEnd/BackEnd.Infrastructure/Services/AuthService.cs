﻿using System;
using Microsoft.AspNetCore.Http;

namespace BackEnd.Infrastructure.Services
{
    public class AuthService: IAuthService
    {
        private readonly IHttpContextAccessor _context;

        public AuthService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public string GetUserIdentity()
        {
            return _context.HttpContext.User.FindFirst("sub").Value;
        }

        public string GetUserName()
        {
            return _context.HttpContext.User.Identity.Name;
        }
    }
}
