﻿namespace BackEnd.Infrastructure
{
    public class ContextSettings
    {
        public string ConnectionString { get; set; }
    }
}
