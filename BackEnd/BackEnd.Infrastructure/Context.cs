﻿using Microsoft.EntityFrameworkCore;
using BackEnd.Infrastructure.Entities;

namespace BackEnd.Infrastructure
{
    public class Context: DbContext
    {
        public Context(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cat>().HasMany(v => v.Votes).WithOne(c => c.Cat).IsRequired();
        }

        public DbSet<Cat> Cats { get; set; }
        public DbSet<Vote> Votes { get; set; }
    }
}
