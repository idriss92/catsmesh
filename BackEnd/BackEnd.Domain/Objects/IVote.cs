﻿using System;

namespace BackEnd.Domain.Objects
{
    public interface IVote
    {
        DateTime VoteDateUtc { get; set; }
        Guid CatId { get; set; }
    }
}
