﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BackEnd.Domain.Objects
{
    public class VoteCollection
    {
        private IList<IVote> _votes;

        public VoteCollection()
        {
            _votes = new List<IVote>();
        }

        public void Add(IVote vote)
        {
            _votes.Add(vote);
        }

        public void Add(IEnumerable<IVote> votes)
        {
            foreach (var vote in votes)
            {
                Add(vote);
            }
        }

        public IReadOnlyCollection<IVote> GetVotes()
        {
            return new ReadOnlyCollection<IVote>(_votes);
        }
    }
}