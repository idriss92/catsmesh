﻿using System;

namespace BackEnd.Domain.Objects
{
    public sealed class Vote: IEntity, IVote
    {
        public Guid Id { get; private set; }
        public DateTime VoteDateUtc { get; set; }
        public Guid CatId { get; set; }

        public static Vote Load(Guid id, Guid catId, DateTime actionDate)
        {
            Vote vote = new Vote {Id = id, CatId = catId, VoteDateUtc = actionDate};
            return vote;

        }
    }
}
