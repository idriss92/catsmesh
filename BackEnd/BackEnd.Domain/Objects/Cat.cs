﻿using System;
using System.Collections.Generic;

namespace BackEnd.Domain.Objects
{
    public sealed class Cat: IEntity,IAggregateRoot
    {
        private VoteCollection _votes;
        public Guid Id { get; set; }
        public string GeneseId { get; set; }
        public string Url { get; set; }
        public int Votes { get; set; }
        Cat() { }

        public IReadOnlyCollection<IVote> GetVotes()
        {
            return _votes.GetVotes();
        }
        public Cat(Guid guid, string geneseId, string url)
        {
            GeneseId = geneseId;
            Id = guid;
            Url = url;
            _votes = new VoteCollection();
        }
        public static Cat Load(Guid id, string geneseId, string url, VoteCollection votes)
        {
            Cat cat = new Cat
            {
                Id = id,
                GeneseId = geneseId,
                _votes = votes,
                Url = url,
                Votes = votes.GetVotes().Count
            };
            return cat;
        }
    }
}
