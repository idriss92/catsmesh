﻿using System;
using System.Collections.Generic;
using BackEnd.Domain.ValueObjects;
using Cat = BackEnd.Domain.Objects.Cat;

namespace BackEnd.Domain
{
    public class CoreCenter
    {
        public IEnumerable<Cat> ShuffleCats(Cat[] cats)
        {
            Random random = new Random();
            List<KeyValuePair<int, Cat>> randomizeCats = new List<KeyValuePair<int, Cat>>();

            foreach (var cat in cats)
            {
                randomizeCats.Add(new KeyValuePair<int, Cat>(random.Next(), cat));
            }

            return randomizeCats.OrderedCats();
        }


        public IEnumerable<BiCat> CreateVersus(Queue<Cat> cats)
        {
            int sizeQueue = cats.Count;
            int totalVersusSize = sizeQueue / 2;
            var biCats = new List<BiCat>();
            do
            {
                biCats.Add(new BiCat(cats.Dequeue(), cats.Dequeue()));
                totalVersusSize--;
            } while (totalVersusSize > 0);

            return biCats;
        }
    }
}