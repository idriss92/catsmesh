﻿namespace BackEnd.Domain.ValueObjects
{
    public class Cat
    {
        public string Id { get; set; }
        public string Url { get; set; }
    }
}
