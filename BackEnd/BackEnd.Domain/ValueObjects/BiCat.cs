﻿using BackEnd.Domain.Objects;

namespace BackEnd.Domain.ValueObjects
{
    public class BiCat
    {
        public BiCat(Cat left, Cat right)
        {
            LeftCat = left;
            RightCat = right;
        }
        public Cat LeftCat { get; set; }
        public Cat RightCat { get; set; }
    }
}
