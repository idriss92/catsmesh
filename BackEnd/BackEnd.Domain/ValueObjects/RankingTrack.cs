﻿namespace BackEnd.Domain.ValueObjects
{
    public class RankingTrack
    {
        public RankingTrack(string catId, int votes)
        {
            CatId = catId;
            Votes = votes;
        }

        public string CatId { get; set; }
        public int Votes { get; set; }
    }
}
