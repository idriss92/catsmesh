﻿using System.Collections.Generic;

namespace BackEnd.Domain.ValueObjects
{
    public class Cats
    {
        public ICollection<Cat> Images { get; set; }
    }
}
