﻿using System;

namespace BackEnd.Domain
{
    internal interface IEntity
    {
        Guid Id { get; }
    }
}
