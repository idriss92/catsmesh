﻿using System.Collections.Generic;
using System.Linq;
using BackEnd.Domain.Objects;

namespace BackEnd.Domain
{
    public static class Extensions
    {
        public static IEnumerable<Cat> OrderedCats(this List<KeyValuePair<int, Cat>> cats)
        {
            return (from cat in cats orderby cat.Key select cat.Value);
        }
    }
}
