﻿using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Commands.FilteringCatRanking;
using BackEnd.Application.Repositories;
using BackEnd.Domain.Objects;
using FizzWare.NBuilder;
using Moq;
using Xunit;

namespace BackEnd.UnitTests.Application
{
    public class FilteringCatRankingCommand
    {
        private readonly Mock<IExternalCatReadOnlyRepository> _externalCatReadOnlyRepository;
        private readonly IFilteringRankingUseCase _filteringRankingUseCase;

        public FilteringCatRankingCommand()
        {
            _externalCatReadOnlyRepository = new Mock<IExternalCatReadOnlyRepository>();
            _filteringRankingUseCase = new FilteringRankingUseCase(_externalCatReadOnlyRepository.Object);
        }

        [Fact]
        public async void FilteringRankingUseCase_Execute()
        {
            int numberOfVote = 4;
            int size = 10;
            var cats = Builder<Cat>.CreateListOfSize(size).TheFirst(5).With(x => x.Votes = numberOfVote).Build().ToList();
            _externalCatReadOnlyRepository.Setup(x => x.GetCatsByNumberOfVote(numberOfVote))
                .Returns(Task.FromResult(cats.Where(x => x.Votes == numberOfVote)));

            var result = await _filteringRankingUseCase.Execute(numberOfVote);

            Assert.Equal(5, result.Count());
        }
    }
}
