﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Commands.InitDashboard;
using BackEnd.Application.Repositories;
using BackEnd.Domain.Objects;
using Moq;
using Xunit;

namespace BackEnd.UnitTests.Application
{
    public class InitDashboardCommand
    {
        private readonly Mock<IExternalCatReadOnlyRepository> _externalCatReadOnlyRepository;
        private readonly InitDashboardUseCase _initDashboardUseCase;

        public InitDashboardCommand()
        {
            _externalCatReadOnlyRepository = new Mock<IExternalCatReadOnlyRepository>();
            _initDashboardUseCase = new InitDashboardUseCase(_externalCatReadOnlyRepository.Object);
        }

        [Fact]
        public async void InitDashboardCommand_Execute_ReturnBitCatQueue()
        {
            int EXPECTED_LENGTH = 3;
            IEnumerable<Cat> cats = new[] {new Cat(Guid.NewGuid(), "catId", "https://path/image.pnf"), new Cat(Guid.NewGuid(), "catId2", "https://path/image.pnf"), new Cat(Guid.NewGuid(), "catId3", "https://path/image.pnf") };
            _externalCatReadOnlyRepository.Setup(x => x.GetCats()).Returns(Task.FromResult(cats));

            var result = await _initDashboardUseCase.Execute();

            Assert.NotNull(result);
            Assert.Equal(EXPECTED_LENGTH, cats.Count());
            _externalCatReadOnlyRepository.Verify(x => x.GetCats(),Times.Once);
        }
    }
}
