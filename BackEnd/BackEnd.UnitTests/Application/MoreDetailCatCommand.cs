﻿using System;
using System.Threading.Tasks;
using BackEnd.Application.Commands.MoreDetailCat;
using BackEnd.Application.Repositories;
using BackEnd.Domain.Objects;
using Moq;
using Xunit;

namespace BackEnd.UnitTests.Application
{
    public class MoreDetailCatCommand
    {
        private readonly Mock<IExternalCatReadOnlyRepository> _externalCatReadOnlyRepository;
        private readonly IDetailOnCatUseCase _detailOnCatUse;

        public MoreDetailCatCommand()
        {
            _externalCatReadOnlyRepository = new Mock<IExternalCatReadOnlyRepository>();
            _detailOnCatUse = new DetailOnCatUseCase(_externalCatReadOnlyRepository.Object);
        }

        [Fact]
        public async void DetailOnCatUserCase_Execute_ReturnNull()
        {
            string CAT_ID = null;
            
            var result = await _detailOnCatUse.Execute(CAT_ID);

            Assert.Null(result);
        }

        [Fact]
        public async void DetailOnCatUserCase_Execute_ReturnExpectedCat()
        {
            string CAT_ID = "catId";
            Cat cat = new Cat(Guid.NewGuid(), CAT_ID, "http://path/image.png");
            _externalCatReadOnlyRepository.Setup(x => x.GetCat(CAT_ID)).Returns(Task.FromResult<Cat>(cat));

            var result = await _detailOnCatUse.Execute(CAT_ID);

            Assert.NotNull(result);
        }
    }
}
