﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Application.Commands.TopCatRanking;
using BackEnd.Application.Repositories;
using BackEnd.Domain.Objects;
using Moq;
using Xunit;

namespace BackEnd.UnitTests.Application
{
    public class RankingUseCaseCommand
    {
        private readonly Mock<IExternalCatReadOnlyRepository> _externalCatReadOnlyRepository;
        private readonly Mock<IVoteReadOnlyRepository> _voteReadOnlyRepository;
        private readonly IRankingUseCase _rankinUseCase;
        public RankingUseCaseCommand()
        {
            _externalCatReadOnlyRepository = new Mock<IExternalCatReadOnlyRepository>();
            _voteReadOnlyRepository = new Mock<IVoteReadOnlyRepository>();
            _rankinUseCase = new RankingUseCase(_voteReadOnlyRepository.Object, _externalCatReadOnlyRepository.Object);
        }

        [Fact]
        public async void RankingUseCase_Execute_ReturnResult()
        {
            int EXPECTED_CAT = 3;
            Guid catId = Guid.NewGuid();
            Guid catId2 = Guid.NewGuid();
            Guid catId3 = Guid.NewGuid();
            IEnumerable<Vote> votes = new []
            {
            Vote.Load(Guid.NewGuid(), catId, DateTime.Now), Vote.Load(Guid.NewGuid(), catId2, DateTime.Now),
                Vote.Load(Guid.NewGuid(), catId3, DateTime.Now)
            };
            VoteCollection voteCollection = new VoteCollection();
            voteCollection.Add(votes);
            IEnumerable<Cat> cats = new[] { Cat.Load(Guid.NewGuid(),"catId", "https://path/image.pnf", voteCollection), Cat.Load(Guid.NewGuid(), "catId2", "https://path/image.pnf", voteCollection), Cat.Load(Guid.NewGuid(), "catId3", "https://path/image.pnf", voteCollection) };
            _externalCatReadOnlyRepository.Setup(x => x.GetCats()).Returns(Task.FromResult(cats));
            
            var result = await _rankinUseCase.Execute();

            Assert.NotNull(result);
            Assert.Equal(EXPECTED_CAT, result.ToList().Count);
        }
    }
}
