﻿using System;
using BackEnd.Application.Commands.ChooseCat;
using BackEnd.Application.Repositories;
using Moq;
using Xunit;

namespace BackEnd.UnitTests.Application
{
    public class ChooseCatCommand
    {
        private readonly Mock<IVoteWriteOnlyRepository> _voteWriteOnlyRepository;
        private IChooseCatUseCase _chooseCatUseCase;
        public ChooseCatCommand()
        {
            _voteWriteOnlyRepository = new Mock<IVoteWriteOnlyRepository>();
            _chooseCatUseCase = new ChooseCatUseCase(_voteWriteOnlyRepository.Object);
        }

        [Fact]
        public void ChooseCatCommand_Execute_ThrowException()
        {
            Assert.ThrowsAsync<Exception>(() => _chooseCatUseCase.Execute(string.Empty, Guid.Empty));
        }

        [Fact]
        public async void ChooseCatCommand_Execute_ReturnAddVoteResult()
        {
            string CAT_ID = "catID";
            Guid userGuid = Guid.NewGuid();

            var result = await _chooseCatUseCase.Execute(CAT_ID, userGuid);

            Assert.IsType<AddVoteResult>(result);
        }
    }
}
