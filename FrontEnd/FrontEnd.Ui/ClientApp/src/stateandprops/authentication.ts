export interface ILoginState {
    email: string;
    password: string;
}

export interface IRegisterState {
    email: string;
    password: string;
    confirmPassword: string;
    isLoading: boolean;
}
