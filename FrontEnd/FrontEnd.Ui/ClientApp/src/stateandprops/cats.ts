﻿import {ICatVersus} from '../models/cats.model';

export interface ICatsState {
    cats: ICatVersus[];
    loading: boolean;
    errors: {};
    selectedIndex: number;
}

export const initialState: ICatsState = {
    loading: false,
    cats: [],
    errors: {},
    selectedIndex: 0,
};
