import { IRanking } from "src/models/ranking.model";

export interface IRankingState {
    votes: IRanking[];
    loading: boolean;
    errors: {};
}

export const initialState: IRankingState = {
    loading: false,
    votes: [],
    errors: {},
};
