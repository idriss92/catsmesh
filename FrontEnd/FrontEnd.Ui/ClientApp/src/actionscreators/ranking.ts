import { LOAD_RANKING, LOAD_RANKING_FAIL, LOAD_RANKING_SUCCESS } from "../constants/ranking";
import { AppThunkAction } from '../reducers';
import { KnowRankingAction } from '../actions/ranking';
import BackEndService from 'src/services/backend.service';

const backendService = new BackEndService();

export const actionCreators = {
    load: (): AppThunkAction<KnowRankingAction> => (dispatch, getState) => {
        dispatch({ type: LOAD_RANKING });
        backendService.getRanking()
            .then(response => {
                dispatch({ type: LOAD_RANKING_SUCCESS, payload: response });
            })
            .catch(error => {
                dispatch({ type: LOAD_RANKING_FAIL, payload: error });
            });
    },
    filterByNumberOfVote: (numberOfVote: any): AppThunkAction<KnowRankingAction> => (dispatch, getState) => {
        dispatch({ type: LOAD_RANKING });
        backendService.getFilteredRanking(numberOfVote)
        .then(response => {
            dispatch({ type: LOAD_RANKING_SUCCESS, payload: response });
        })
        .catch(error => {
            dispatch({ type: LOAD_RANKING_FAIL, payload: error });
        });
    },
};
