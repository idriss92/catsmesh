import { LOAD_CATS, LOAD_CATS_SUCCESS, LOAD_CATS_FAIL, CHOOSE_CAT, CHOOSE_CAT_SUCCESS, CHOOSE_CAT_FAIL, INCREMENT_INDEX_CATS } from "../constants/cats";
import { AppThunkAction } from '../reducers';
import { KnowCatsAction } from '../actions/cats';
import BackEndService from 'src/services/backend.service';

const backendService = new BackEndService();
export const actionCreators = {
    load: (): AppThunkAction<KnowCatsAction> => (dispatch, getState) => {
        dispatch({ type: LOAD_CATS });
        backendService.getCats()
            .then(response => {
                dispatch({ type: LOAD_CATS_SUCCESS, payload: response });
            })
            .catch(error => {
                dispatch({ type: LOAD_CATS_FAIL, payload: error });
            });
    },
    vote: (catId: any): AppThunkAction<KnowCatsAction> => (dispatch, getState) => {
        dispatch({ type: CHOOSE_CAT });
        backendService.chooseCat(catId)
            .then(response => {
                dispatch({ type: CHOOSE_CAT_SUCCESS });
                dispatch({ type: INCREMENT_INDEX_CATS, payload: 1});
            })
            .catch(error => {
                dispatch({ type: CHOOSE_CAT_FAIL, payload: error });
            });
    },
};
