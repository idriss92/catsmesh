"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var cats_1 = require("../constants/cats");
exports.actionCreators = {
    load: function (url) { return function (dispatch, getState) {
        console.log(url);
        dispatch({ type: cats_1.LOAD_CATS, isLoading: true });
        axios_1.default.get(url)
            .then(function (response) {
            console.log(response.data);
            dispatch({ type: cats_1.LOAD_CATS_SUCCESS, payload: response.data });
        })
            .catch(function (error) {
            dispatch({ type: cats_1.LOAD_CATS_FAIL, payload: error });
        });
    }; },
};
//# sourceMappingURL=cats.js.map