"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StorageService = /** @class */ (function () {
    function StorageService() {
    }
    StorageService.prototype.retrive = function (key) {
        var item = localStorage.getItem(key);
        if (item && item !== 'undefined') {
            return JSON.parse(item);
        }
    };
    StorageService.prototype.store = function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };
    return StorageService;
}());
exports.default = StorageService;
//# sourceMappingURL=storage.service.js.map