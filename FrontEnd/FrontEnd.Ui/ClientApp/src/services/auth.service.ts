import axios from 'axios';
import StorageService from './storage.service';

export default class AuthService {
    storageService: StorageService;
    headers: Headers;

    constructor() {
        this.storageService = new StorageService();
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    login(email: string, password: string): any {
        const authUrl = this.storageService.retrive("authUrl");
        const authLoginUrl = `${authUrl}connect/token`;
        return axios.post(authLoginUrl, {
            client_id: 'backend',
            client_secret: 'secret',
            grant_type: 'password',
            username: email,
            password,
        }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
            });
    }

    register(email: string, password: string, confirmPassword): any {
        const authUrl = this.storageService.retrive("authUrl");
        const authLoginUrl = `${authUrl}api/account/register`;
        return axios.post(authLoginUrl, {
            email,
            password,
            confirmPassword,
        }, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });
    }

    getToken(): any {
        return this.storageService.retrive("bearer_token");
    }

    logOff() {
        this.storageService.store("bearer_token", "");
    }

    setHeaders() {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        const token = this.getToken();

        if (token !== '') {
            this.headers.append('Authorization', 'Bearer ' + token);
        }
    }
}
