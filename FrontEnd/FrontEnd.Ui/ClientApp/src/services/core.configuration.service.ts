﻿import axios from 'axios';
import StorageService from './storage.service';
import { ICoreConfiguration } from '../models/coreconfiguration.model';

export default class CoreConfigurationService {
    storageService: StorageService;

    constructor() {
        this.storageService = new StorageService();
    }

    load(): void {
        const baseURI = document.baseURI.endsWith('/') ? document.baseURI : `${document.baseURI}`;
        const url = `${baseURI}api/Home`;
        axios.get(url).then(response => {
            console.log(response.data as ICoreConfiguration);
            this.storageService.store("backEndUrl", (response.data as ICoreConfiguration).backEndUrl);
            this.storageService.store("authUrl", (response.data as ICoreConfiguration).authUrl);
        });
    }

    generateUuid(): void {
        let dt = new Date().getTime();
        const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            // tslint:disable-next-line:no-bitwise
            const r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            // tslint:disable-next-line:no-bitwise
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        this.storageService.store('x-requestId', uuid);
    }
}
