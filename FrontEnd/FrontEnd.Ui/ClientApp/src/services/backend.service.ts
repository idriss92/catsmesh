import StorageService from './storage.service';
import axios from 'axios';
import { ICatVersus } from "../models/cats.model";
import { IRanking } from "../models/ranking.model";

export default class BackEndService {

    private backendUrl: string;
    private storageService: StorageService;
    private rankingUrl: string;
    private catUrl: string;
    private requestId: string;

    constructor() {
        this.storageService = new StorageService();
        this.requestId = this.storageService.retrive('x-requestId');
        this.backendUrl = this.storageService.retrive("backEndUrl");
        this.rankingUrl = `${this.backendUrl}/api/Votes`;
        this.catUrl = `${this.backendUrl}/api/Cats`;
    }

    getCats(): Promise<ICatVersus[]> {
        return axios.get(this.catUrl).then(response => {
            return response.data as ICatVersus[];
        });
    }

    chooseCat(catId: any) {
        return axios.post(this.rankingUrl, JSON.stringify(catId), {
            headers: {
                'accept': 'application/json',
                'x-requestId': this.requestId,
                'Content-Type': 'application/json-patch+json',
            },
        });
    }

    getRanking(): Promise<IRanking[]> {
        return axios.get(this.rankingUrl).then(response => {
            return (response.data as IRanking[]).sort();
        });
    }

    getFilteredRanking(numberOfVote: any): Promise<IRanking[]> {
        return axios.get(`${this.rankingUrl}/${numberOfVote}`)
        .then(response => {
            return (response.data as IRanking[]).sort();
        });
    }
}
