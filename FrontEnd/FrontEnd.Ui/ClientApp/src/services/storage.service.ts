export default class StorageService {
    retrive(key: any): any {
        const item = localStorage.getItem(key);
        if (item && item !== 'undefined') {
            return JSON.parse(item);
        }
    }

    store(key: any, value: any): void {
        localStorage.setItem(key, JSON.stringify(value));
    }
}
