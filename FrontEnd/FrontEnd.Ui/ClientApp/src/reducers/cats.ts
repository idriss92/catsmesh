﻿import { initialState } from "../stateandprops/cats";
import { LOAD_CATS, LOAD_CATS_FAIL, LOAD_CATS_SUCCESS, INCREMENT_INDEX_CATS, CHOOSE_CAT_FAIL, CHOOSE_CAT, CHOOSE_CAT_SUCCESS } from "../constants/cats";

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case LOAD_CATS:
            return {
                ...state,
                loading: true,
            };
        case LOAD_CATS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        case LOAD_CATS_SUCCESS:
            return {
                ...state,
                loading: false,
                cats: action.payload,
            };
        case INCREMENT_INDEX_CATS:
            return {
                ...state,
                selectedIndex: state.selectedIndex + action.payload,
            };
            case CHOOSE_CAT:
            return {
                ...state,
                loading: true,
            };
            case CHOOSE_CAT_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
            case CHOOSE_CAT_SUCCESS:
            return {
                ...state,
                loading: false,
            };
        default:
            return initialState;
    }
};
