﻿import { ICatsState } from '../stateandprops/cats';
import * as catsReducer from './cats';
import { IRankingState } from 'src/stateandprops/ranking';
import * as rankingReducer from './ranking';

export interface IApplicationState {
    cats: ICatsState;
    ranking: IRankingState
}

export const reducers = {
    cats: catsReducer.reducer,
    ranking: rankingReducer.reducer,
};

export type AppThunkAction<TAction> = (dispatch: (action: TAction) => void, getState: () => IApplicationState) => void;
