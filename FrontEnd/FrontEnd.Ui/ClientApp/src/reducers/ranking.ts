import { initialState } from "../stateandprops/ranking";
import { LOAD_RANKING, LOAD_RANKING_FAIL, LOAD_RANKING_SUCCESS } from '../constants/ranking';

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case LOAD_RANKING:
            return {
                ...state,
                loading: true,
            };
        case LOAD_RANKING_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        case LOAD_RANKING_SUCCESS:
            return {
                ...state,
                loading: false,
                votes: action.payload,
            };
        default:
            return initialState;
    }
};
