"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var cats_1 = require("../stateandprops/cats");
var cats_2 = require("../constants/cats");
exports.reducer = function (state, action) {
    state = state || cats_1.initialState;
    switch (action.type) {
        case cats_2.LOAD_CATS:
            return __assign({}, state, { loading: true });
        case cats_2.LOAD_CATS_FAIL:
            return __assign({}, state, { loading: false, error: action.payload });
        case cats_2.LOAD_CATS_SUCCESS:
            return __assign({}, state, { loading: false, cats: action.payload });
        default:
            return cats_1.initialState;
    }
};
//# sourceMappingURL=cats.js.map