"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_thunk_1 = require("redux-thunk");
var react_router_redux_1 = require("react-router-redux");
var Cats = require("../reducers/cats");
var Ranking = require("../reducers/ranking");
function configureStore(history, initialState) {
    var reducers = {
        cats: Cats.reducer,
        ranking: Ranking.reducer,
    };
    var middleware = [
        redux_thunk_1.default,
        react_router_redux_1.routerMiddleware(history),
    ];
    // In development, use the browser's Redux dev tools extension if installed
    var enhancers = [];
    var isDevelopment = process.env.NODE_ENV === 'development';
    if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
        enhancers.push(window.devToolsExtension());
    }
    var rootReducer = redux_1.combineReducers(__assign({}, reducers, { routing: react_router_redux_1.routerReducer }));
    return redux_1.createStore(rootReducer, initialState, redux_1.compose.apply(void 0, [redux_1.applyMiddleware.apply(void 0, middleware)].concat(enhancers)));
}
exports.default = configureStore;
//# sourceMappingURL=configureStore.js.map