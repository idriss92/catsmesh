﻿import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Ranking from './components/Ranking';
import Cats from './components/Cats';
import Login from './components/Auth/Login/Login';
// import Register from './components/Auth/Register/Register';

export default () => (
  <Layout>
    <Route exact path='/' component={Cats} />
    <Route path='/ranking' component={Ranking} />
    <Route path='/login' components={Login} />
    {/* <Route path='/register' components={Register} /> */}
  </Layout>
);
