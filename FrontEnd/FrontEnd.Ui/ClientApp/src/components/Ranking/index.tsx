import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { IRankingState } from '../../stateandprops/ranking';
import { actionCreators } from "../../actionscreators/ranking";
import { IApplicationState } from "../../reducers/index";
import FilterRanking from './FilterRanking';
import Rankings from './Rankings';

type RankingProps = IRankingState & typeof actionCreators;

class Ranking extends React.Component<RankingProps, any> {

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        await this.props.load();
    }

    render() {
        const { loading, votes, filterByNumberOfVote, load } = this.props;

        return (
            <div>
                <FilterRanking filterByNumberOfVote={filterByNumberOfVote} load={load} />
                <Rankings loading={loading} votes={votes} />
            </div>
        );
    }
}

export default connect(
    (state: IApplicationState) => state.ranking,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(Ranking);
