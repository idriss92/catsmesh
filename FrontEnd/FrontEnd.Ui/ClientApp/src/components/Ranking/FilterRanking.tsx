import * as React from 'react';
import { FormControl, Form, Button, Col, Row } from 'react-bootstrap';

export default class FilterRanking extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            vote: '',
        };
    }

    filterByVote = (event: any) => {
        this.setState({ vote: event.target.value });
        this.props.filterByNumberOfVote(event.target.value);
    }

    reset = () => {
        this.setState({ vote: ''});
        this.props.load();
    }

    render() {
        return (
            <div style={{ paddingTop: '50px' }}>
                <Row className="justify-content-md-center">
                    <Form inline>
                        <Row>
                            <Col sm="7">
                                <FormControl type='number' value={this.state.vote} min={1} placeholder='Enter the number' onChange={this.filterByVote} />
                            </Col>
                            <Col sm="5">
                                <Button variant="outline-success" onClick={this.reset}>Reset search</Button>
                            </Col>
                        </Row>
                    </Form>
                </Row>
            </div>
        );
    }
}
