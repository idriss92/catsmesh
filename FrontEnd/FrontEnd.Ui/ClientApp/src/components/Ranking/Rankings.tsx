import * as React from 'react';
import { CardColumns, Card, Spinner, Alert } from 'react-bootstrap';

export default class Rankings extends React.Component<any, any> {

    constructor(props) {
        super(props);
    }

    render() {
        const { loading, votes } = this.props;
        if (loading) {
            return (
                <Spinner animation="grow" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            );
        }
        if (votes.length > 0) {
            return (
                <div style={{ paddingTop: '50px' }}>
                    <CardColumns>
                        {votes.map(vote => {
                            return (
                                <Card key={vote.geneseId}>
                                    <Card.Img key={vote.geneseId} height='600' width='550' variant="top" src={vote.url} />
                                    <Card.Body key={vote.geneseId}>
                                        <Card.Footer key={vote.geneseId}>
                                            <small className="text-muted">{vote.votes} votes</small>
                                        </Card.Footer>
                                    </Card.Body>
                                </Card>
                            );
                        })}
                    </CardColumns>
                </div>
            );
        }
        return (
            <div style={{ paddingTop: '150px' }}>
                <Alert dismissible variant="danger">
                    <Alert.Heading>Null content</Alert.Heading>
                    <p>
                        Your search don't give data
                    </p>
                </Alert>;
            </div>
        );
    }
}
