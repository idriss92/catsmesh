import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ICatsState } from '../../stateandprops/cats';
import { actionCreators } from "../../actionscreators/cats";
import { IApplicationState } from "../../reducers/index";
import BiCat from './BiCat';

type CatsProps = ICatsState & typeof actionCreators;

class Cats extends React.Component<CatsProps, any> {

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        await this.props.load();
    }

    chooseCat = (event: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
        const catId = event.currentTarget.id;
        this.props.vote(catId);
    }

    render() {
        const { cats, loading, selectedIndex } = this.props;
        if (cats.length > 0 && selectedIndex < cats.length) {
            return (
                <div style={{ paddingTop: '70px' }}>
                <BiCat selectedIndex={selectedIndex} cats={cats} chooseCat={this.chooseCat} loading={loading} />
                </div>
            );
        }
        return (
            <div>
            </div>
        );
    }
}

export default connect(
    (state: IApplicationState) => state.cats,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(Cats);
