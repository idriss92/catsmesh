import * as React from 'react';
import { Card, Row, Col, Figure, Spinner } from 'react-bootstrap';

export default class BiCat extends React.Component<any, any> {
    constructor(props) {
        super(props);
    }

    render() {
        const { selectedIndex, cats, chooseCat, loading } = this.props;
        if (loading) {
            return (
                <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            );
        }
        return (
            <div>
                <Row className="justify-content-md-center">
                    <Figure>
                        <Figure.Image width={171} height={1180} rounded={true} src="https://bohmmarrazzo.files.wordpress.com/2012/08/080112wc5_0361.jpg" />
                        <Figure.Caption>{(selectedIndex === cats.length) ? "No more cats to vote" : "Choose the champions cat"}</Figure.Caption>
                    </Figure>
                </Row>
                <Row>
                    <Col sm={6}>
                        <Card >
                            <Card.Img height='500' width='450' onClick={chooseCat} id={cats[selectedIndex].leftCat.geneseId} variant="top" src={cats[selectedIndex].leftCat.url} />
                        </Card>
                    </Col>
                    <Col sm={6}>
                        <Card>
                            <Card.Img height='500' width='450' onClick={chooseCat} id={cats[selectedIndex].rightCat.geneseId} variant="top" src={cats[selectedIndex].rightCat.url} />
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}
