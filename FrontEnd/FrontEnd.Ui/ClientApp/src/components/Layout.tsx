import * as React from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import ConfigurationService from '../services/core.configuration.service';
import NavMenu from './NavMenu';

export default class Layout extends React.PureComponent {

    configurationService: ConfigurationService;

    constructor(props) {
        super(props);
        this.configurationService = new ConfigurationService();
    }

    componentDidMount() {
        this.configurationService.load();
        this.configurationService.generateUuid();
    }

    render() {
        return (
            <div>
                <NavMenu />
                <Container fluid>
                    <Row>
                        <Col sm={12}>
                            {this.props.children}
                        </Col>
                    </Row>
                </Container></div>
        );
    }
}
