import React, { Component } from "react";
import { Form } from "react-bootstrap";
import "./Register.css";
import { IRegisterState } from '../../../stateandprops/authentication';

export default class Register extends Component<any, IRegisterState>  {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            email: "",
            password: "",
            confirmPassword: "",
        };
    }

    validateForm() {
        return (
            this.state.email.length > 0 &&
            this.state.password.length > 0 &&
            this.state.password === this.state.confirmPassword
        );
    }

    validateConfirmationForm() {
        // return this.state.confirmationCode.length > 0;
    }

    handleChange = event => {
        // this.setState({
        //     [event.target.id]: event.target.value
        // });
    }

    handleSubmit = async event => {
        event.preventDefault();

        this.setState({ isLoading: true });

        // this.setState({ newUser: "test" });

        this.setState({ isLoading: false });
    }

    handleConfirmationSubmit = async event => {
        event.preventDefault();

        this.setState({ isLoading: true });
    }

    renderForm() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        autoFocus
                        type="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                    />
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        value={this.state.password}
                        onChange={this.handleChange}
                        type="password"
                    />
                </Form.Group>
                <Form.Group controlId="confirmPassword">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        value={this.state.confirmPassword}
                        onChange={this.handleChange}
                        type="password"
                    />
                </Form.Group>
            </Form>
        );
    }

    componentDidMount() {
        console.log("hello");
    }

    render() {
        return (
            <div className="Register">
            zer
                {this.renderForm()}
            </div>
        );
    }
}
