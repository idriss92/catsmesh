import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from "../../../actionscreators/ranking";
import { IApplicationState } from "../../../reducers/index";
import { IRankingState } from 'src/stateandprops/ranking';
import Login from './Login';

type RankingProps = IRankingState & typeof actionCreators;

class LoginIndex extends React.Component<RankingProps, any> {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // tslint:disable-next-line:no-debugger
        debugger;
        console.log("Login is mount");
    }

    render() {
        return (
            <div>
                <Login />
            </div>
        );
    }
}

export default connect(
    (state: IApplicationState) => state.ranking,
    dispatch => bindActionCreators(actionCreators, dispatch),
)(LoginIndex);
