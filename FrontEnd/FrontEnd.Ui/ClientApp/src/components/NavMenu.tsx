﻿import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

export default props => (
  <Navbar bg="primary" variant="dark">
    <Navbar.Brand>CatsMesh</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <LinkContainer to={'/'} exact>
          <Nav.Link>
            Cats
          </Nav.Link>
        </LinkContainer>
        <LinkContainer to={'/ranking'}>
          <Nav.Link>
            Ranking
          </Nav.Link>
        </LinkContainer>
        {/* <LinkContainer to={'/register'} exact>
          <Nav.Link>
            Register
          </Nav.Link>
        </LinkContainer> */}
        <LinkContainer to={'/login'} exact>
          <Nav.Link>
            Login
          </Nav.Link>
        </LinkContainer>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
