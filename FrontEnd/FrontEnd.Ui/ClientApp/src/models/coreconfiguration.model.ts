export interface ICoreConfiguration {
    backEndUrl: string;
    authUrl: string;
}
