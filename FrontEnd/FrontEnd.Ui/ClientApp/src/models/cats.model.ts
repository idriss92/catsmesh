﻿export interface ICat {
    id: any;
    url: any;
    geneseId: any;
}

export interface ICatVersus {
    rightCat: ICat;
    leftCat: ICat;
}
