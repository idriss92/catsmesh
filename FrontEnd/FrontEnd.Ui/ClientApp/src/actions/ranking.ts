import { IRanking } from '../models/ranking.model';

interface ILoadRanking { type: 'LOAD_RANKING'; }
interface ILoadRankingFail { type: 'LOAD_RANKING_FAIL'; payload: {} }
interface ILoadRankingSuccess { type: 'LOAD_RANKING_SUCCESS'; payload: IRanking[]; }

export type KnowRankingAction = ILoadRanking |
    ILoadRankingFail |
    ILoadRankingSuccess;
