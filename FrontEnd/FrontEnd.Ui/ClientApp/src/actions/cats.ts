﻿import { ICatVersus } from '../models/cats.model';

interface ILoadCats { type: 'LOAD_CATS'; }
interface ILoadCatsFail { type: 'LOAD_CATS_FAIL'; payload: {} }
interface ILoadCatsSuccess { type: 'LOAD_CATS_SUCCESS'; payload: ICatVersus[]; }
interface IIncrementIndexCats { type: 'INCREMENT_INDEX_CATS'; payload: number }
interface IChooseCat { type: "CHOOSE_CAT"; }
interface IChooseCatFail { type: "CHOOSE_CAT_FAIL"; payload: {}; }
interface IChooseCatSuccess { type: "CHOOSE_CAT_SUCCESS"; }

export type KnowCatsAction = ILoadCats |
    ILoadCatsFail |
    ILoadCatsSuccess |
    IIncrementIndexCats |
    IChooseCat |
    IChooseCatFail |
    IChooseCatSuccess;
