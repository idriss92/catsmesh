﻿namespace FrontEnd.Ui
{
    public class AppSettings
    {
        public string BackEndUrl { get; set; }
        public string AuthUrl { get; set; }
    }
}
