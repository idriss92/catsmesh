﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FrontEnd.Ui.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private IHostingEnvironment _environment;
        private IOptionsSnapshot<AppSettings> _settings;

        public HomeController(IHostingEnvironment environment, IOptionsSnapshot<AppSettings> settings)
        {
            _environment = environment;
            _settings = settings;
        }
        // GET: api/Home
        [HttpGet]
        public IActionResult Configuration()
        {
            return Ok(_settings.Value);
            //return Json(_settings.Value);
        }
    }
}